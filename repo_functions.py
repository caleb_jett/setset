import os
import json
import math
import pygit2
from tabulate import tabulate
def cloneRepo():
    if os.path.isdir("./part-directory"):
        if os.path.isdir("./part-directory/.git"):
            print("fetching updates...")
            os.system("cd part-directory & git pull & cd ..")
        else:
            print("cloning git directory...")
            os.system("git clone https://github.com/MST-Robotics/part-directory")
    else:
        print("making repo directory...")
        os.mkdir("./part-directory")
        cloneRepo()

def uploadChanges(sys_config):
    os.system("cd part-directory & git stage partdb.json & cd ..")
    os.system("cd part-directory & git commit -m \"auto - update database\"")
    os.system("cd part-directory & git push & cd ..")

def loadParts(repo_directory):
    try:
        with open(os.path.join(repo_directory,"partdb.json"), "r") as read_file:
            part_dict = json.load(read_file)
    except FileNotFoundError:
        print("Error: could not find partdb.json!")
        exit()
    else:
        print("{} entries loaded".format(len(part_dict["entries"])))
    return part_dict

def saveParts(part_dict,repo_directory):
    try:
        with open(os.path.join(repo_directory,"partdb.json"), "w") as write_file:
            json.dump(part_dict,write_file,indent="\t")
    except FileNotFoundError:
        print("Error: could not find partdb.json!")
        SystemExit()
    else:
        print("{} entries saved".format(len(part_dict["entries"])))

def listParts(part_dict):
    for part in part_dict:
        print(part_dict["parts"][part]["nick"])

def initParts(part_dict,sys_config):
    if(input("Are you sure? This will DELETE ALL EXISTING PARTS AND SETS!  (\"yes\"/[anything else])")=="yes"):
        part_dict = sys_config["default_config"]
    return part_dict

def addPart(part_dict):
    part_dict_backup = part_dict
    entry_num = str(len(part_dict["entries"])+1)
    category = ""
    part_id = ""
    while part_id == "":
        print("What part category does this part fall under?")
        for cat in part_dict["cats"]:
            print("\t"+cat+"\t-\t"+part_dict["cats"][cat]["desc"])
        resp = input("type the designator for the category, or \"cancel\":")
        if resp.lower()=="cancel": return part_dict
        for cat in part_dict["cats"]:
            if resp.lower()==cat.lower(): 
                part_id = resp.upper()+str(part_dict["cats"][cat]["num_parts"]+1)
                category = cat
        if part_id == "": print("invalid category. ")
    print("creating part ID "+part_id+"["+entry_num+"]")

    nickname = input("part Nickname: ")
    description = input("part Description: ")
    manufacturer = input("manufacturer: ")
    manufacturerPN = input("manufacturer PN: ")
    cost = input("cost per 1 unit: ")
    link = input("link: ")
    stock = int(input("current stock: "))

    if(input("is this a releasted part?  (\"yes\"/[anything else])")=="yes"):
        rev = "1.0.0"
    else:
        rev = "0.0.1"

    if input("Submit part? (\"yes\"/[anything else])")=="yes":
        part_dict["entries"][entry_num]=dict(id=part_id,
                                             nick=nickname,
                                             desc=description,
                                             mfg=manufacturer,
                                             pn=manufacturerPN,
                                             cost=cost,
                                             link=link,
                                             stock=stock,
                                             rev = rev)
        part_dict["cats"][category]["num_parts"]+=1

def addSet(part_dict):
    part_dict_backup = part_dict
    entry_num = str(len(part_dict["entries"])+1)
    set_id = "SET"+str(part_dict["num_sets"]+1)
    print("Adding "+set_id+"["+entry_num+"]")
    nickname = input("set Nickname: ")
    description = input("set Description: ")
    if(input("is this a releasted set?  (\"yes\"/[anything else])")=="yes"):
        rev = "1.0.0"
    else:
        rev = "0.0.1"
    resp = "temp"
    line_item = 0
    entries = {}
    while True:
        resp_entry = input("entry OR '' to finish: ").lower()
        if resp_entry == "": break
        if resp_entry == "search": print("search not implemented yet")
        entry = "0"
        try: int(resp_entry)
        except:
            print("unsupported yet")
        else:
            entry = resp_entry
        try: 
            entries[entry]
        except:
            try:
                part_dict["entries"][entry]
            except:
                print("entry does not exist!")
            else:
                while True:
                    resp_value = input("quantity: ")
                    if resp_value == "": break
                    try: qty = int(resp_value)
                    except: print("invalid response")
                    else:
                        break
                entries[str(line_item)]={"num":resp_entry,"qty":qty}
                line_item+=1
        else: print("entry exists in set already!")
        
        

    if input("Submit set? (\"yes\"/[anything else])")=="yes":
        part_dict["entries"][entry_num]=dict(id=set_id,nick=nickname, desc=description, rev = rev)
        part_dict["entries"][entry_num]["entries"] = entries
        part_dict["num_sets"]+=1

def dispEntryList(part_dict,entry_list):
    displayWindow = Tk()
    table = []
    entry_count = 0
    for entry in entry_list:
        line = Text()
        line.insert("1.0",entry)
        line.insert("2.0","\t")
        line.insert("3.0",part_dict["entries"][entry]["id"])
        line.insert("4.0","\t")
        line.insert("5.0",part_dict["entries"][entry]["nick"])
        table+=[line]
    for row in range(len(table)):
        table[row].grid(row=row,column=0)
    displayWindow.mainloop()
        
    table = []
    for entry in entry_list:
        table+=[[entry,part_dict["entries"][entry]["id"],part_dict["entries"][entry]["nick"]]]
    print(tabulate(table, headers=['entry#', 'ID', 'nickname']))


def whereUsed(part_dict, entry_num):
    usedList = []
    for entry in part_dict["entries"]:
        set_entries = part_dict["entries"][entry].get("entries",None)
        if not set_entries == None:
            if usedInSet(part_dict, part_dict["entries"][entry]["entries"], entry_num):
                usedList +=entry
    return usedList


def usedInSet(part_dict, set_entries, entry_num):
    for entry in set_entries:
        if set_entries[entry]["num"] == entry_num: return True
        subset_entries = part_dict["entries"][set_entries[entry]["num"]].get("entries",None)
        if not subset_entries == None:
            return usedInSet(part_dict,subset_entries,entry_num)


def displayEntry(part_dict, entry_num):
    table = []
    print("")
    for key in part_dict["entries"][entry_num]:
        if not key == "entries":
            table+=[[key,part_dict["entries"][entry_num][key]]]
    print(tabulate(table, headers=['Key', 'Value']))
    table = []
    if "entries" in part_dict["entries"][entry_num]:
        for entry in part_dict["entries"][entry_num]["entries"]:
            info = part_dict["entries"][entry_num]["entries"][entry]
            table+=[[entry, info["num"],part_dict["entries"][info["num"]]["id"],part_dict["entries"][info["num"]]["nick"],info["qty"]]]
        print("")
        print(tabulate(table, headers=['Line Item', 'Entry #', 'ID', 'nickname', 'quantity'], tablefmt='orgtbl'))
    print("")

def searchParts(search_terms):
    global part_dict
    results = []
    for part in part_dict:
        add_part = False
        for term in search_terms:
            print((part_dict[part])["tags"])
            print(search_terms[term]["key"])
            print((part_dict[part]["tags"])[[search_terms[term]["key"]]])
            if part_dict[part]["tags"][[search_terms[term]["key"]]] == search_terms[term]["value"]:
                add_part = True
        if add_part:
            results = results+part
    return results
        



