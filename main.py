import json
from repo_functions import *
import os
import shutil
import sys
from tkinter import *
import ctypes
import stat
from bs4 import BeautifulSoup
import urllib
from github import Github,BadCredentialsException
from git import Repo

repo_directory = os.path.join(os.getcwd(),"part-directory")
shell_help = {
    "entry":"(not a command) everywhere this is requested, entry# OR part/set ID [version (optional)] OR 'search' may be given",
    "help":"display a list of commands",
    "reload":"reloads the program",
    "add":"adds a new part to the working temporary database",
    "adds":"add a collection of parts/sets",
    "save":"saves the working database",
    "upload":"upload parts to github",
    "whereUsed [entry]":"find everywhere a part or set is used",
    "disp [entry]":"display an entry",
    "exit":"close the program",
    "fin":"saves changes, uploads them, and exits program"
}

def loadConfig():
    with open("config.json", "r") as read_file:
        return json.load(read_file)

def saveConfig(sys_config):
    with open("config.json", "w") as write_file:
        json.dump(sys_config,write_file,indent="\t")

def searchParts(search_terms):
    global part_dict
    results = []
    for part in part_dict:
        add_part = False
        for term in search_terms:
            print((part_dict[part])["tags"])
            print(search_terms[term]["key"])
            print((part_dict[part]["tags"])[[search_terms[term]["key"]]])
            if part_dict[part]["tags"][[search_terms[term]["key"]]] == search_terms[term]["value"]:
                add_part = True
        if add_part:
            results = results+part
    return results
        
def importMMCPart():
    tempURL = "https://www.mcmaster.com/94180a331"
    page=urllib.request.urlopen(tempURL)
    pageSoup = BeautifulSoup(page, 'html.parser')
    
    tbl = pageSoup.find('div', id="ShellLayout_Cntnr")
    tbl = tbl.find('div', id = "ShellLayout_Content_Cntnr")
    tbl = tbl.find('div', id = "ShellLayout_MainContent_Cntnr")
    tbl = tbl.find('div', id = "ShellLayout_MainContent_Inner")
    tbl = tbl.find('div', id = "MainContent")
    print(tbl)
    tbl = tbl.find('div')
    print(tbl)

def shellHelp():
    for command in shell_help:
        print("\t"+command+"\t-\t"+shell_help[command])

def startup():
    print("///////////////////////////////////////////////////")
    sys_config = loadConfig()
    cloneRepo()

    part_dict = loadParts(repo_directory)
    shell(part_dict, repo_directory, sys_config)

def shell(part_dict, repo_directory, sys_config):
    print("///////////////////////////////////////////////////")
    print("\nType a command, or \"help\" for a list of commands.")
    while True:
        resp = input(">>").lower()
        if resp.find(" ")>0:
            arg = resp[resp.find(" ")+1:]
            resp = resp[0:(resp.find(" "))]
        if resp == "help": shellHelp()
        if resp == "add": addPart(part_dict)
        if resp == "save": saveParts(part_dict,repo_directory)
        if resp == "upload": uploadChanges(sys_config)
        if resp == "reload":
            startup()
            return
        if resp == "exit": return
        if resp == "fin":
            saveParts(part_dict,repo_directory)
            uploadChanges(sys_config)
            return
        if resp == "adds": addSet(part_dict)
        if resp == "whereused": dispEntryList(part_dict,whereUsed(part_dict,arg))
        if resp == "disp": displayEntry(part_dict,arg)
  
#new_part_button = Button(text="New Part")
#new_part_button.pack()  
startup()
#importMMCPart()
#loadConfig()
#initGithub()
#cloneRepo()
#listParts()
#part_dict = addPart(part_dict)
#addPart(part_dict)
#listParts(part_dict)
#saveParts(part_dict,repo_directory)
#print(searchParts(search_terms))
#uploadChanges()
#shell()